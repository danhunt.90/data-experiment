-- SnowSQL script to load the data to a stage in Snowflake, assuming it's in your Downloads folder on a Mac
-- See docs: https://docs.snowflake.com/en/user-guide/data-load-local-file-system-stage.html
use database raw;
use schema stages_local;

put 'file:///Users/<YOUR_MAC_USER>/Downloads/Takeout/Location History/Records.json' @JSON_STAGE;
