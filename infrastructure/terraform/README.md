## First time terraform setup

### 1. Create a key for your user to authenticate
Create a public key for this user to authenticate to Snowflake:
```zsh
cd ~/.ssh
openssl genrsa 2048 | openssl pkcs8 -topk8 -inform PEM -out snowflake_terraform_key.p8 -nocrypt
openssl rsa -in snowflake_terraform_key.p8 -pubout -out snowflake_terraform_key.pub
```

You should now have a keypair under `~/.ssh`, and we'll use the public key when creating our user. See your public key:
```zsh
cat ~/.ssh/snowflake_terraform_key.pub
```

You will need the part of the key between the header and footer:

```
-----BEGIN PUBLIC KEY-----  <- ignore
<your_public_key>           <- copy including newlines
-----END PUBLIC KEY-----    <- ignore
```

### 2. Create a terraform user in Snowflake
Now that you have a key to authenticate, let's create a user that will be used to define objects in your Snowflake account based on your Terraform config.

Open a SnowSQL session or the Snowsight web console and run the following commands using your copied key from above:
```sql
create role terraform;

-- we want this role to be able to create resources across our account
grant role sysadmin to role terraform;
grant role securityadmin to role terraform;
grant role useradmin to role terraform;

-- create a corresponding user linked to your public key
create user tf_user rsa_public_key='<your_public_key>' default_role=public must_change_password=false;

-- link the role (permissions) to the user (credentials)
grant role terraform to user tf_user;
alter user tf_user set default_role=terraform;
```

Your terraform user is now set up!

### 3. Set up the credentials for your user to authenticate
Get your account locator from your account URL. For example, and account URL containing `https://lp12345.eu-west-2.aws.snowflakecomputing.com` would give a value of `"lp12345.eu-west-2.aws"`.


Create a file `terraform.tfvars` file with the following parameters, including the account locator your account URL above:
```
snowflake_user = "TF_USER"
snowflake_private_key_path = "~/.ssh/snowflake_terraform_key.p8"
snowflake_account = ""
```

These values will now be loaded by default into your Terraform environment, avoiding the need to hardcode values.

### 4. Initialize terraform
To install the required providers, which control how terraform defines its resources, run the following from this folder:
```zsh
terraform init
```

You can now test that the environment is working by running:
```zsh
terraform plan
```

This command should not return any errors - if it does, try deleting the `terraform.tfstate` file and try again.
