# Role for loading data into the `raw` database and transforming data in the `analytics` database
resource "snowflake_role" "datahunt" {
  name = "DATAHUNT"
}