# grant the loader role permissions to add to the JSON stage
resource "snowflake_file_format_grant" "json_file_format_usage" {
  database_name    = snowflake_database.raw.name
  schema_name      = snowflake_schema.stages_local.name
  file_format_name = snowflake_file_format.json_file_format.name

  privilege = "USAGE"
  roles = [
    snowflake_role.datahunt.name
  ]
}