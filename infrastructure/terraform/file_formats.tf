resource "snowflake_file_format" "json_file_format" {
  name     = "JSON_FILE_FORMAT"
  database = snowflake_database.raw.name
  schema   = snowflake_schema.stages_local.name

  format_type    = "JSON"
  file_extension = "AUTO"
  compression    = "NONE"

  lifecycle {
    ignore_changes = [
      binary_format,
      date_format,
      skip_byte_order_mark,
      time_format,
      timestamp_format
    ]
  }
}
