
# grant select on future tables in raw.stages_local for loader, transformer
resource "snowflake_table_grant" "raw_stages_local_select_future_tables" {
  database_name = snowflake_database.raw.name
  schema_name   = snowflake_schema.stages_local.name
  privilege     = "SELECT"
  roles         = [snowflake_role.datahunt.name]
  on_future     = true
}


# grant select on future views in raw.stages_local for loader, transformer
resource "snowflake_view_grant" "raw_stages_local_select_future_views" {
  database_name = snowflake_database.raw.name
  schema_name   = snowflake_schema.stages_local.name
  privilege     = "SELECT"
  roles         = [snowflake_role.datahunt.name]
  on_future     = true
}
