# permissions on the `analytics` database
resource "snowflake_database_grant" "analytics_usage" {
  database_name = snowflake_database.analytics.name
  privilege     = "USAGE"
  roles = [
    snowflake_role.datahunt.name
  ]
}

resource "snowflake_database_grant" "analytics_create_schema" {
  database_name = snowflake_database.analytics.name
  privilege     = "CREATE SCHEMA"
  roles = [
    snowflake_role.datahunt.name
  ]
}

# permissions on the `raw` database
resource "snowflake_database_grant" "raw_usage" {
  database_name = snowflake_database.raw.name
  privilege     = "USAGE"
  roles = [
    snowflake_role.datahunt.name
  ]
}

resource "snowflake_database_grant" "raw_create_schema" {
  database_name = snowflake_database.raw.name
  privilege     = "CREATE SCHEMA"
  roles = [
    snowflake_role.datahunt.name
  ]
}
