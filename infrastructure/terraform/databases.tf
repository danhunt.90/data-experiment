# database to hold transformed data
# this is where transformations will be performed, by analysts
# or transformation tools such as dbt
resource "snowflake_database" "analytics" {
  name = "ANALYTICS"
}

# database to hold unmodified source data
# read-only for downstream purposes
# generally append-only access pattern for loader roles
resource "snowflake_database" "raw" {
  name = "RAW"
}