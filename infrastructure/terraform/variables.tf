variable "snowflake_account" {
    description = "The account locator for the target Snowflake account, e.g. 'LP1234'"
    type = string
}

variable "snowflake_user" {
    description = "The Snowflake user to be used by Terraform. This user should have access to SYSADMIN and USERADMIN roles."
    type = string
}

variable "snowflake_private_key_path" {
    description = "The local path to the private key used to authenticate to Snowflake, e.g. '~/.ssh/snowflake_terraform_key.p8'"
    type = string
}