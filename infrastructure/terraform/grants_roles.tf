resource "snowflake_role_grants" "datahunt" {
  role_name = snowflake_role.datahunt.name
  users = [
    snowflake_user.user_datahunt.name
  ]
}
