# data-experiment
Build tool models to convert raw data from various systems into analytical tables used for analysis and visualisation.

## Getting set up with dbt
This repository integrates with build tool [dbt](https://docs.getdbt.com/), which creates directed acyclic graphs (DAGs) of data models based on their dependencies, and can be used to build or update these tables from source.

### Installation
Initial installation is as simple as running a few commands, and then setting up your local profiles file, which will contain confidential information to connect to our data warehouse under your personal schema. If you went through our onboarding process, you should have run the script in our `hello-world` [repo](https://github.com/biblio-tech/hello-world), which installs `dbt` for you. If you haven't run this, installation is as simple as a few commands with `brew`:

```zsh
brew tap fishtown-analytics/dbt
brew install dbt
```

This project also uses Terraform: ensure you install it by running the below commands
```zsh
brew tap hashicorp/tap
brew install hashicorp/tap/terraform
```

You should now be able to configure your profile.

### Configuring `~/.dbt/profile.yml`
In order to be able to prototype in your own safe sandbox environment, you'll need to configure a local file for `dbt` to use to determine the database and schema to connect to. We develop locally using only a `dev` target schema - `prod` should only ever be used by our CI environment to build the customer-facing versions of our models.

If the file doesn't exist yet, create a blank template as follows:
```zsh
mkdir ~/.dbt
touch ~/.dbt/profiles.yml
```

You'll need to initialise your terraform project before setting up your dbt profile - go to `/infrastructure/terraform/README.md` for instructions on how to do so.

Once set up, log into the Snowflake app into the ETL_USER account and default password, provided in the `/infrastructure/terraform/users.tf` config. You will be prompted to change the password upon first login - choose a secure password, which you will use in your local dbt profile configuration.

Create a profile in this format, using the password you just created for the ETL_USER, and your Snowflake account details:
```
data_hunt:
  target: transformer
  outputs:
    transformer:
      type: snowflake
      account: 
      user: ELT_USER
      password: 
      role: DATAHUNT
      database: RAW
      warehouse: TRANSFORMING
      schema: STAGES_LOCAL
      threads: 4
  ```

### Configuring SSL connectivity
Since AWS updated their SSL connectivity requirements, it is now necessary to download and configure a SSL certificate to access the database used by this project. To do so, download the certifcate from [this link](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/UsingWithRDS.SSL.html) and save it in an appropriate folder on your computer (recommend within the .ssh folder in your home\users\user_name directory).

Once downloaded, update your .env file, and set the parameter value 'SSL_CERTIFICATE' to the location of the certificate (including certiciate name). You will also need to update the database connection in other apps (i.e. DBeaver). 

To do so in DBeaver, edit the database connection, then navigate to the 'SSL' tab in connection settings. Enter your SSL certificate location against the 'Root certificate' field.

### Load your location data into the Snowflake stage
Before you can start modelling with dbt, you'll need to load up your Google location data into the Snowflake stage set up by terraform. Start by downloading your Google data (instructions [here](https://support.google.com/accounts/answer/3024190?hl=en)). Unzip the file in your downloads (or optionally save it in some local drive). Load the data from your local machine into the stage by running the SQL sripts in the `scripts/loaders/google/location.sql` file. Ensure you're running these commands as the ETL_USER, and adjust the file path to point to the location of your unzipped Google data.

Once the data has been loaded, you can validate it has been loaded correctly by viewing the stage contents:
`list @JSON_STAGE;`
