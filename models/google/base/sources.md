# Description of data transformations within base models
This document describes the transformations applied to Google data within these base models

{% docs google_location %}

# Google location data
This has been split into a view and a table:
  - base_location - view. This contains the raw location data, with no structural changes. The only difference is that this view does not contain the 'activity' column, as it is captured in a table (below). The granularity of this table is one row per timestamp
  - base_activity - table. This contains the data stored in a column ('activity') within the raw google location data. Activity data was stored in a nested json format, which has been 'unnested' in this table. The granularity of this table is one row per 'type' of activity captured per timestamp

  {% enddocs %}