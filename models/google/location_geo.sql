{{
  config({
    "materialized" : "table"
    })
}}

with location as (
  select * from {{ ref('base_location') }}
),

-- add some useful aggregate columns
location_utililty_columns as (
  select
    *
    
    -- round location to a practical level for visualisation
    , round(latitude::decimal,3) as rounded_latitude
    , round(longitude::decimal,3) as rounded_longitude

    -- round date to different levels for averaging and visualisation
    , date_trunc('week', location_at) as location_date_week
    -- 30min seems like a good balance to capture most locations visited
    , time_slice(location_at, 30, 'minute', 'start') as location_at_30min_interval
  from location
)

select 
  *

  -- create a geometric representation to allow GIS, e.g. nearest city
  , ST_MakePoint(longitude, latitude) as location_geo
from location_utililty_columns