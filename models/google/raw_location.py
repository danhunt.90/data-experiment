import gzip
from pandas import DataFrame
from ijson import items

def model(dbt, session):
    dbt.config(
        materialized = "table",
        packages = ["snowflake-snowpark-python","pandas","ijson"],
        snowflake_warehouse='LOADING' # snowpark-optimized warehouse which is better for python workloads
    )

    # get the location data file to make it available to this function
    DATA_PATH = '/tmp'
    LOCATION_DATA_FILENAME = 'Records.json.gz'
    LOCATION_DATA_STAGE_PATH = session.file.get('@json_stage/' + LOCATION_DATA_FILENAME,  DATA_PATH)[0].file

    with gzip.open(DATA_PATH + '/' + LOCATION_DATA_STAGE_PATH, 'r') as file:
        # stream the data from only the specified top-level attribute
        locations = items(file, 'locations.item')
        
        # define the columns we want to extract and filter the data
        columns = ['latitudeE7', 'longitudeE7', 'accuracy', 'source', 'timestamp', 'activity']
        filtered_locations = [[item[col] for col in columns] for item in locations if all(k in item for k in columns)]
        
        # extract the requested attribute and convert to a Snowpark data frame
        pd_df = DataFrame(filtered_locations, columns = columns)

        # create a Snowpark Dataframe from the Pandas data frame
        df = session.create_dataframe(pd_df)
    
    return df
