{{
  config({
    "materialized" : "table"
    })
}}

with location as (
  select * from {{ ref('location_geo') }}
),

anchor_ranges as (
  select * from {{ ref('location_anchor_ranges') }}
),

final as (
  select * from location
  left join anchor_ranges using ( location_date_week )
)

select * from final