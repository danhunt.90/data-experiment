{% docs seed_world_cities_geo %}
Location data for all cities in the world with a population of more than 15,000 people, as well as all cities with the country codes `AU` and `GB` (which are relevant to the authors). Data includes the latitude and longitude, as well as the relevant city, region, and country. Geo data is calculated only for the central point at which the city is located, as opposed to any kind of shape or bounding area.

This data is intended for use in combination with other location datasets, primarily to find the closest city to a given point.

Note: some cities in this dataset are listed at the exact same geographic location (latitude and longitude). In these cases, which are uncommon but still notable, the "cities" involved tend to be small towns and it is assumed that any of the alternatives will generally suffice.

{% enddocs %}


{% docs country_iso_code %}
Two-digit ISO country code for this location, e.g. 'AU', 'GB', 'US'.
{% enddocs %}

{% docs region %}
Localised region code for this city, e.g. 'P8'. This may not be easily interpretable.
{% enddocs %}

{% docs city_ascii %}
City name, lowercased and mapped to the ASCII character set. This should only be used when UTF-8 is not available.
{% enddocs %}

{% docs city %}
City name in the UTF-8 character set.
{% enddocs %}

{% docs population %}
Approximate city population.

In rare cases this may not align with the city: specifically, in the case of very small towns at the exact same geographic location (latitude, longitude), the sum of the population of these towns will be used.
{% enddocs %}
