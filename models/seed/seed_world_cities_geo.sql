{{
  config({
    "materialized" : "table"
    })
}}

with world_cities as (
  select 
    Country as country_iso_code
    , Latitude as latitude
    , Longitude as longitude

    -- only keep one city per latitude, longitude pair
    --  => this is accurate enough for our purposes even if an approximation
    , max(City) as city_ascii
    , max(AccentCity) as city -- utf-8 version with non-ascii chars, use by default
    , max(Region) as region
    
    -- for towns at the exact same location, take the sum of populations
    , sum(Population) as population -- filtered at source csv to > 15k or in AU / GB
  from {{ ref('world_cities_gt_15k') }}
  {{ dbt_utils.group_by(3) }}
)

select 
  *

  -- create a geometric representation to allow GIS, e.g. nearest city
  , ST_MakePoint(longitude, latitude) as location_geo

  -- unique surrogate id - ultimately, location is what should be unique
  , {{ dbt_utils.generate_surrogate_key(['latitude', 'longitude']) }} as unique_id
from world_cities