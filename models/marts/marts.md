{% docs location_by_rounded_coordinates %}

This model performs some aggregation and rounding of location data to make it fit for visualisation and consumption. Location latitude and longitude is rounded to 3 decimal places, as this captures the granularity of a change in roughly one small city block (~111m), which is deemed granular enough for these purposes.

The main transformation that this model performs is grouping by rounded latitude and rounded longitude, capturing a count of records and average accuracy, average velocity, and earliest and latest records at this co-ordinate group. The model is materialised as a table.

{% enddocs %}

{% docs location_by_30min_interval %}

Presents location data for visualisation with an emphasis on the temporal component - when a place was visited. Data is presented in 30min intervals, which significantly reduces data volume (to ~10-15% row count) while maintaining practical usability even within a day level (e.g. home / work / activity visualisation).

In addition to truncated time data, rounded latitude and longitude values are used to provide consistency and group location data to a practical city-block level.

{% enddocs %}